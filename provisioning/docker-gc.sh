#!/bin/bash

set -o errexit

function installDockerGC {
	# Clone the source project
	git clone https://github.com/spotify/docker-gc
	# Build the rpm package
	pushd docker-gc
	sudo debuild -us -uc -b
	popd
	# Install the package
	sudo dpkg -i docker-gc_*_all.deb
	# Create the cron job
	sudo cat << EOF | sudo tee /etc/cron.hourly/docker-gc
#!/bin/bash
/usr/sbin/docker-gc
EOF
	sudo chmod +x /etc/cron.hourly/docker-gc
}

echo "Provisioning docker-gc"

installDockerGC

echo "docker-gc Successfully Provisioned"
