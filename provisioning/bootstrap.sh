#!/bin/bash

set -o errexit

function installSoftwareBase {
	# Install software base
	# Install build essential
	sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test
  sudo apt-get update
	sudo apt-get install -y devscripts debhelper build-essential dh-make
	# Upgrade the system
	sudo apt-get upgrade -y
	# Install Git
	sudo apt-get install -y git
	git config --global user.name "Vagrant Machine"
	git config --global user.email "vagrant-machine@domain.com"
	# Configure git. Disable SSL Check
	sudo cat << EOF | sudo tee /etc/gitconfig
[http]
  sslVerify = false
EOF
}

echo "Provisioning the Server"

installSoftwareBase

echo "Server Successfully Provisioned"
